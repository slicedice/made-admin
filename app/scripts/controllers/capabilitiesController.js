angular.module('sbAdminApp')
  .controller('CapabilitiesCtrl', function ($scope, $sce, Made) {

    Made.capabilities()
    .then(function(result){
        $scope.services = result;
    });

    $scope.show_editor = false
    $scope.params = {};
    $scope.result = '';
    $scope.schema = {}
    $scope.error_string = '';
    $scope.model = {};
    $scope.has_parameters = false;
    $scope.request_args_string = ''
    $scope.return_doc = ''
    $scope.node = null;
    $scope.form = [
        "*",
        {
            type: "submit",
            title: "Execute"
        }
    ];
    $scope.strip_html = function(text) {
        return text ? String(text).replace(/<[^>]+>/gm, '') : '';
    }
    
    $scope.show_node = function(service) {
        $scope.node = service
    }
    $scope.show_capatibility = function(sub) {
        $scope.node = null
        schema = {
            "title": sub.name,
            "type": "object",
            "properties": {}
        }

        if ('return' in sub.params) {
            $scope.return_doc = JSON.stringify(sub.params['return'], null, 4)
            delete sub.params['return']
        }

        $scope.has_parameters = false;
        for (key in sub.params) {
            schema['properties'][key] = {}
            for (k in sub.params[key]) {
                schema['properties'][key][k] = sub.params[key][k]
            }

            if (sub.params[key]['type'] == 'object' && ! ('properties' in sub.params[key])) {
                schema['properties'][key]['type'] = 'string'
                schema['properties'][key]['description'] = 'JSON OBJECT'
                schema['properties'][key]["x-schema-form"] = {
                    "type": "textarea",
                    "placeholder": "JSON OBJECT"
                  }
            }

            $scope.has_parameters = true;
        }
        $scope.current_sub = sub
        $scope.show_editor = Object.keys(schema['properties']).length > 0
        $scope.request_args_string = ''
        $scope.error_string = ''
        $scope.result = ''
        $scope.schema = schema
    }
    $scope.fire = function(der) {
        _fire({})
    }
    $scope.onSubmit = function(form) {
        $scope.$broadcast('schemaFormValidate');
        sub = $scope.current_sub;
        if (form.$valid) {
            
            data = {}
            for(key in sub.params) {
                var current = sub.params[key];

                if (! $scope.model[key]) {
                    console.log(key)
                    if (current['type'] == 'object') {
                        data[key] = {}
                    }
                    else if (current['type'] == 'string') {
                        data[key] = ''
                    }
                    else if (current['type'] == 'integer') {
                        data[key] = 0
                    }
                    else if (current['type'] == 'float') {
                        data[key] = 0.0
                    }
                    else if (current['type'] == 'boolean') {
                        data[key] = false
                    }
                }
                else if(current['type'] == 'object' && ! ('properties' in current)) {                
                    old_value = $scope.model[key].trim()

                    try {
                        data[key] = JSON.parse(old_value);
                    }
                    catch(e) {
                        $scope.error_string = ''+e
                        return false
                    }
                }
                else {
                    data[key] = $scope.model[key]
                }

            }

            _fire(data)
            
        }
    }

    _fire = function(data) {
        rpc = 'rpc:/'+$scope.current_sub.path;
        $scope.result = ''
        $scope.request_args_string = JSON.stringify(data, null, 4)
        $scope.response_string = ''
        Made.request(rpc, [], data).then(function(result) {
            $scope.error_string = ''
            $scope.result = JSON.stringify(result['data'], null, 4);
        }, function(error) {
            $scope.result = ''
            $scope.error_string = error['error']['message']
        });        
    }

    $scope.trust = function(data) {
        return $sce.trustAsHtml(data);
    }

    $scope.isSubspace = function(sub) {
        return true;
    }

    $scope.isNamespace = function(sub) {
        return sub.type == 'namespace' || sub.type == 'class';
    }

});
