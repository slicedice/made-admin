angular.module('sbAdminApp')
  .controller('LogsCtrl', function ($scope, Made) {

    Made.request('rpc://crm/logs')
    .then(function(result) {
        $scope.logs = result['data'];
    });

});