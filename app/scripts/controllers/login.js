
angular.module('sbAdminApp')
  .controller('LoginCtrl', function($scope, $state, Made) {
        Made.afterConnect(function() {

        });

        $scope.data = {};

        $scope.login = function() {
            Made.loginByName($scope.data.name, $scope.data.password)
             .then(function(result){
                if(result['success']) {
                    $state.go('dashboard.home');
                }
             });
        };
  });
