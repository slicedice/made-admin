'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
  .controller('MainCtrl', function($scope, $position, Made) {
        Made.afterConnect(function() {
            Made.request('rpc://crm/systemstats').then(function(result) {
                $scope.services = result['data']['total_online'];
            });
        });
  });
