angular.module('sbAdminApp')
  .controller('UsersCtrl', function ($scope, $modal, Made) {

    $scope.data = {};
    $scope.group = {};
    $scope.add_user = null;

    function get_users() {
        Made.request('rpc://crm/user/list').then(function(result) {
            $scope.users = result['data'];
        });
    }

    Made.afterConnect(function(){
        get_users();
    });

    $scope.AddUser = function() {
        // remove password check
        delete $scope.data['password2'];

        // hash the password
        $scope.data['password'] = CryptoJS.SHA512($scope.data['password']).toString(CryptoJS.enc.Hex);

        $scope.data['emails'] = [$scope.data['email']];
        delete $scope.data['email'];

        $scope.data['active'] = true;

        Made.request('rpc://crm/user/set', [$scope.data])
            .then(function(result) {
                $scope.data = {};
                $scope.modal.close();

                get_users();
            });
    };

    $scope.ResetPassword = function(user) {
        Made.request('rpc://crm/user/reset_password', [user._id]);
    };

    $scope.DeleteUser = function(user) {
        Made.request('rpc://crm/user/delete', [user._id])
            .then(function(result) {
                get_users();
            });

    };

    $scope.AddGroup = function() {
        Made.request('rpc://crm/user/add_group', [$scope.selected._id, $scope.group.name]);
    };

    $scope.DumpUsers = function() {
        Made.request('rpc://crm/user/dump')
            .then(function(result){
                var file = Made.fileFromData('user.dump.yml.xz', _base64ToArrayBuffer(result['data']));
                file.save();
            });
    };

    $scope.UploadDump = function() {
        console.log($scope.data.file);

        // var data = String.fromCharCode.apply(null, new Uint16Array($scope.data.file[0].data));
        var data = _arrayBufferToBase64($scope.data.file[0].data);

        Made.request('rpc://crm/user/load', [data]).then(function() {
            setTimeout(get_users, 200);
        });

        $scope.modal.close();
    };

    $scope.open = function(template, selected) {
        $scope.selected = selected;

        $scope.modal = $modal.open({
            animation: $scope.animationsEnabled,
            templateUrl: template,
            scope: $scope,
            // controller: 'ModalInstanceCtrl',
            // size: size,
            // resolve: {
            //     AddUser: function () {
            //         return $scope.items;
            //     }
            // }
        });

        $scope.modal.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.close = function() {
        $scope.modal.close();
    }

    $scope.toggle = function(id) {
        console.log('-- toggle --');
        $('.collapse').collapse('hide');
        $('#'+id).collapse('toggle');
    };
});