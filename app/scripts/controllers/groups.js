angular.module('sbAdminApp')
  .controller('GroupsCtrl', function ($scope, $modal, Made) {

    $scope.data = {};

    function get_groups() {
        Made.request('rpc://crm/group/list').then(function(result) {
            $scope.groups = result['data'];
        });
    }

    Made.afterConnect(function(){
        get_groups();
    });

    $scope.AddGroup = function() {
        Made.request('rpc://crm/group/add', [$scope.data.name])
            .then(function(result) {
                $scope.data = {};
                $scope.modal.close();

                get_groups();
            });
    };

    $scope.DeleteGroup = function(group) {
        Made.request('rpc://crm/group/delete', [group._id])
            .then(function(result) {
                get_groups();
            });

    };

    $scope.DumpGroups = function() {
        Made.request('rpc://crm/group/dump')
            .then(function(result){
                var file = Made.fileFromData('group.dump.yml.xz', _base64ToArrayBuffer(result['data']));
                file.save();
            });
    };

    $scope.UploadDump = function() {
        console.log($scope.data.file);

        // var data = String.fromCharCode.apply(null, new Uint16Array($scope.data.file[0].data));
        var data = _arrayBufferToBase64($scope.data.file[0].data);

        Made.request('rpc://crm/group/load', [data]).then(function() {
            setTimeout(get_groups, 200);
        });

        $scope.modal.close();
    };

    $scope.open = function(template) {

        $scope.modal = $modal.open({
            animation: $scope.animationsEnabled,
            templateUrl: template,
            scope: $scope,
            // controller: 'ModalInstanceCtrl',
            // size: size,
            // resolve: {
            //     AddUser: function () {
            //         return $scope.items;
            //     }
            // }
        });

        $scope.modal.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.toggle = function(id) {
        console.log('-- toggle --');
        $('.collapse').collapse('hide');
        $('#'+id).collapse('toggle');
    };
});