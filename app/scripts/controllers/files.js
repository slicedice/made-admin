angular.module('sbAdminApp')
.controller('FilesCtrl', function($scope, $modal, Made) {

    function list() {
        Made.request('rpc://crm/file/list')
          .then(function(result){
              $scope.files = result['data'];
          });
    }

    Made.afterConnect(function() {
        list();
    });

    $scope.data = {};

    $scope.upload = function() {
        var fobj = $scope.data.file[0];
        console.log(fobj);

        fobj.store()
            .then(function(result) {
                list();

                $scope.file_upload.close();
            });
    };

    $scope.setname = function() {
        console.log($scope.data.file[0]);
        $scope.data.name = $scope.data.file[0].filename;
    };

    $scope.open = function(size) {

        $scope.file_upload = $modal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'views/file_upload.html',
            scope: $scope,
            // controller: 'ModalInstanceCtrl',
            // size: size,
            // resolve: {
            //     AddUser: function () {
            //         return $scope.items;
            //     }
            // }
        });

        $scope.file_upload.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.close = function() {
        $scope.file_upload.close();
    }

    $scope.toggle = function(id) {
        $('.collapse').collapse('hide');
        $('#'+id).collapse('toggle');
    };

    $scope.save = function(fobj) {
        var file = File(Made, fobj);
        file.readAll()
            .then(file.save);
    }

    $scope.delete = function(fobj) {
        Made.request('rpc://crm/file/remove', [], {'id': fobj['_id']})
            .then(list);
    }
});