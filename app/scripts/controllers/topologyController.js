angular.module('sbAdminApp')
  .controller('TopologyCtrl', function ($scope, Made) {

    Made.afterConnect(function(){
        Made.topology().then(function(data) {
            $scope.data = data;
        });
    });

});