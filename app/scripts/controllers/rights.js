angular.module('sbAdminApp')
.controller('RightsCtrl', function($scope, $modal, Made) {

    function list() {
        Made.request('rpc://crm/accessrights/list')
          .then(function(result){
              $scope.rights = result['data'];

              for (var i = $scope.rights.length - 1; i >= 0; i--) {
                  var resource = $scope.rights[i];

                  $scope.access[resource._id] = {};

                  for (var j = resource.rights.length - 1; j >= 0; j--) {
                      var right = resource.rights[j];

                      $scope.access[resource._id][right.group] = {};

                      for (var k = right.access.length - 1; k >= 0; k--) {
                            var access = right.access[k];
                            $scope.access[resource._id][right.group][access] = true;
                      };
                  };
              };
          });
    }

    Made.afterConnect(function() {
        list();
    });

    $scope.data = {};
    $scope.rights = {};
    $scope.access = {};
    $scope.selected = null;

    $scope.AddRight = function() {
        Made.request('rpc://crm/accessrights/create', [$scope.data.uri])
            .then(function(result) {
                list();
                $scope.modal.close();
            });
    };

    $scope.DeleteRight = function(right) {
        Made.request('rpc://crm/accessrights/delete', [right._id])
            .then(function(result) {
                list();
            });
    };

    $scope.AddGroup = function() {
        Made.request('rpc://crm/accessrights/add_group', [$scope.selected._id, $scope.data.group, 0])
            .then(function(){
                list();
                $scope.modal.close();
            });
    };

    $scope.RemoveGroup = function(rid, gid) {
        Made.request('rpc://crm/accessrights/remove_group', [rid, gid]);
    };

    $scope.SaveGroup = function(rid, gid) {
        var access = [];
        var rights = ['read', 'write', 'execute', 'create', 'delete'];

        for (var i = rights.length - 1; i >= 0; i--) {
            var right = rights[i];

            if($scope.access[rid][gid][right]) {
                access.push(right);
            }
        };

        Made.request('rpc://crm/accessrights/modefy_group', [rid, gid, access]);
    };

    $scope.open = function(template, selected) {

        $scope.selected = selected;

        $scope.modal = $modal.open({
            animation: $scope.animationsEnabled,
            templateUrl: template,
            scope: $scope,
            // controller: 'ModalInstanceCtrl',
            // size: size,
            // resolve: {
            //     AddUser: function () {
            //         return $scope.items;
            //     }
            // }
        });

        $scope.modal.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            console.log('Modal dismissed at: ' + new Date());
        });
    };

    $scope.close = function() {
        $scope.modal.close();
    };

    $scope.toggle = function(id) {
        console.log('-- toggle --');
        $('.collapse').collapse('hide');
        $('#'+id).collapse('toggle');
    };
});
