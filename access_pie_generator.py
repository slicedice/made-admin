#-*- coding: utf-8 -*-
"""
:Author: Arne Simon [arne.simon@slice-dice.de]
:Copyright: (c) 2015 by Slice + Dice GmbH
"""
import pygal


NONE = 0
READ = 1            #: Allows to read a resource.
WRITE = 2           #: Allows to write to a resource.
CREATE = 4          #: Allows to create or to instanciate.
DELETE = 8          #: Allows to delete or destroy an instance.
EXECUTE = 16        #: Allows to execute or perform something.
ALL = [READ, WRITE, CREATE, DELETE, EXECUTE]


if __name__ == '__main__':
    for i in range(32):
        pie = pygal.Pie()
        pie.render_to_file('/{}.svg')